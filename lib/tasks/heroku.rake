namespace :heroku do
    task :dev do
        deploy_to_target('dev')
    end
    task :preview do
        deploy_to_target('preview')
    end
    task :test do
        deploy_to_target('test')
    end
    task :live do
        deploy_to_target('live')
    end
end

def deploy_to_target(target)
    deploy_branch = 'deploy-targets/' + target
    active_branch = `git rev-parse --abbrev-ref HEAD`
    system 'git checkout ' + deploy_branch
    system 'git merge ' + active_branch
    system 'git rm -rf public/assets'
    system 'RAILS_ENV=production rake assets:clean'
    system 'RAILS_ENV=production rake assets:precompile'
    system 'git add --all .'
    system 'git commit -m "precompiled assets"'
    system 'git checkout ' + active_branch
    puts 'pushing ' + deploy_branch  + ' to ' + target + ' as master'
    system 'git push ' + target + ' ' + deploy_branch + ':master'
end
