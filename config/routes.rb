CitiesApp::Application.routes.draw do
  real_root = '/eumm/home/'
  
  root :to => redirect(real_root)
  match '/eumm/' => redirect(real_root)
  match 'm/browsehappy' => 'home#browsehappy'
  scope( :path => '/eumm') do
    match 'home/' => 'home#home'
    match 'lessons/' => 'home#lessons'
    match 'workshops/' => 'home#workshops'
    match 'contacts/' => 'home#contacts'
    scope( :path => '/m') do
      match 'metromonitor/(:city)' => 'home#metromonitor'
      match 'mr-vs-national/' => 'home#mr-vs-national'
      match 'sectors/' => 'home#sectors'
      match 'resilience/' => 'home#resilience'
      match 'city-types/' => 'home#city-types'
      match 'methods/' => 'home#methods'
    end
  end
  match '*path' => redirect(real_root)
end
