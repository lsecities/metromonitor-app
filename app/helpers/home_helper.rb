module HomeHelper
    def tab_label(label_text)
        render 'shared/tab_label', :label_text => label_text
    end
end
