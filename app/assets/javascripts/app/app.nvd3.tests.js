/**
 * various NVD3 test/work-in-progress code; move to own js file when
 * getting ready for production
 */

// nvd3 graphs: sector growth + sector weight
// using dummy weight data until we have real data
var draw_growth_and_weight_chart = function(data_class) {
    nv.addGraph(function() {
        var chart = nv.models.multiChart()
            .showDistX(true)
            .showDistY(true)
            .color(d3.scale.category10().range());
        
         chart.xAxis
            .tickFormat(d3.format('d'));
        
        chart.yAxis1
            .tickFormat(d3.format('d'));
        
        chart.yAxis2
            .tickFormat(d3.format('%'));
        
        d3.select('#growthplusweight svg')
            .datum(dummy_data_sector_weight(city, data_class))
          .transition().duration(500)
            .call(chart);
        
        return chart; 
    });
}

// nvd3 graphs: scatterplot (TBD)
var draw_scatter_plot = function(data_class) {
    nv.addGraph(function() {
        var chart = nv.models.scatterChart()
            .showDistX(true)
            .showDistY(true)
            .color(d3.scale.category10().range());

        chart.xAxis.tickFormat(d3.format('.02f'))
        chart.yAxis.tickFormat(d3.format('.02f'))

        d3.select('#chart svg')
            .datum(dummy_data_sector_weight(city, data_class))
          .transition().duration(500)
            .call(chart);

        nv.utils.windowResize(chart.update);

        return chart;
    });
}


function dummy_data_sector_weight(this_city, data_class) {
    return _.map(sector_series, function(series) {
        dummy_data = dummy_data_percent_matrix(5);
        return {
            values: _.map(data_years, function(year) {
                // OLD DATASET
                // return { x: year, y: dummy_data[data_class  + '_' + series.code + '_' + year] }; // DATASET: EMP|GDP_SERIES_YYYY -- EMP|GVA_Total_YYYY
                // NEW DATASET
                return { x: year, y: dummy_data[data_class  + '_' + series.code + '_' + year] }; // DATASET: EMP|GDP_SERIES_YYYY -- EMP|GVA_SERIES_YYYY
                
                // var city_data = _.find(dataset.features, function (city) { return city.id === this_city });
                // return { x: year, y: city_data.properties[data_class + '_' + series.code + '_' + year] }; // DATASET: EMP|GDP_SERIES_YYYY
            }),
            key: series.label,
            yAxis: 2,
            type: 'bar'
        };
    });
}

// used to generate dummy sector weight data
function dummy_data_percent_matrix(sector_series) {
    return _.map(data_years, function(year) {
        var year_weights = _.map(sector_series, function(series) {
            return { code: series.code, value: Math.floor(Math.random() * (100 - 1 + 1)) + 1 };
        });
        console.log(year_weights);
        var total_weight = _.reduce(year_weights, function(memo, num){ return memo + num.value; }, 0);
        console.log(total_weight);
        var year_weight_percents = _.map(year_weights, function(series, index) {
            return { code: series.code, value: ((series.value / total_weight) * 100) };
        });
        
        console.log(year_weight_percents);
        
        return {
            values: _.map(year_weight_percents, function(value) {
                return {
                    x: year,
                    y: value
                };
            }),
            key: year
        };
    });
}