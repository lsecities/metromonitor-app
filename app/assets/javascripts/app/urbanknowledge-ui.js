// HTML5 Fullscreen API
//
function toggleFullScreen() {
  if (!document.fullscreenElement &&    // alternative standard method
      !document.mozFullScreenElement && !document.webkitFullscreenElement) {  // current working methods
    if (document.documentElement.requestFullscreen) {
      document.documentElement.requestFullscreen();
    } else if (document.documentElement.mozRequestFullScreen) {
      document.documentElement.mozRequestFullScreen();
    } else if (document.documentElement.webkitRequestFullscreen) {
      document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
    }
  } else {
    if (document.cancelFullScreen) {
      document.cancelFullScreen();
    } else if (document.mozCancelFullScreen) {
      document.mozCancelFullScreen();
    } else if (document.webkitCancelFullScreen) {
      document.webkitCancelFullScreen();
    }
  }
}

$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "fade",
    controlNav: false,
    directionNav: false,
    slideshowSpeed: 4000,
    animationSpeed: 2000
  });
});

$("header .tools .fullscreen").click(function() {
    toggleFullScreen();
});

$("#unpack").toggle(function() {
   $("#tabs-index").addClass("hide");
   // $("#unpack a").text("Unpack the crisis").addClass("active");
   $("#unpack a").addClass("active");
}, function() {
   $("#tabs-index").removeClass("hide");
   // $("#unpack a").text("Hide tabs").removeClass("active");
   $("#unpack a").removeClass("active");
});

indicators_selector = "#chartIndicators li a span";
$(indicators_selector).click(function() {
    timepoint = $('#slider').slider('value');
    if($(this).hasClass('indicator-emp')) {
        $(indicators_selector + '.indicator-gdp').removeClass("active");
        $(this).addClass("active");
        active_data_class = 'EMP';
    } else if($(this).hasClass('indicator-gdp')) {
        $(indicators_selector + '.indicator-emp').removeClass("active");
        $(this).addClass("active");
        active_data_class = 'GVA';
    }
    
    performance(dataset, timepoint);
    if(active_city) {
        draw_summary_growth_chart(active_city, active_data_class);
        draw_growth_chart(active_city, sector_series, active_data_class);
        // need to solve earliest data issue in R first
        draw_sector_percent_chart('.sector-percent.initial', active_city, 1998, active_data_class);
        draw_sector_percent_chart('.sector-percent.crisis', active_city, 2007, active_data_class);
        draw_sector_percent_chart('.sector-percent.final', active_city, 2014, active_data_class);
    }
});

$(document).ready(function() {
  /**
   * Make sure D3 plots are redrawn when a tab is shown; otherwise
   * D3 plots are drawn with zero width (not sure whether this is a
   * D3, NVD3, bootstrap, browser bug or just my misunderstanding
   * of how the tabs widget should work).
   */
  $('#myTab a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
    $(window).trigger('resize');
    // reposition_scatterplot();
  });
});

/**
 * This moves scatterplot back up if we are hiding NVD3's legend
 * to show our proxy legend instead.
 */
function reposition_scatterplot() {
    $('.chart > svg > g').attr('transform', 'translate(75, 20)');
}

/*
$('#mrvnational .extras .keys').html(function() {
    var countries_legend = _.map(_.keys(all_countries), function(country_code) {
        return '<dt data-key="' +  country_code + '">' + country_code + '</dt>' + '<dd>' + all_countries[country_code] + '</dd>';
    });
    return '<dl class="dl-horizontal">' + countries_legend.join("\n") + '</dl>';
});

function update_legend_item(index, element) {
    var key = $(element).data('key');
    var sibling_element = $(element).closest('.visualization .core').find('.nv-legend .nv-series').find('text:contains("' + key + '")');
    var fill_color = sibling_element.parent().find('circle').css('fill');
    var stroke_color = sibling_element.parent().find('circle').css('stroke');
    if($(element).find('svg').length === 0) {
        $(element).prepend('<svg xmlns="http://www.w3.org/2000/svg" version="1.1" height="5"><g transform="translate(10,10)"><circle r="5" style="stroke-width: 2px; fill: ' + fill_color + '; stroke: ' + stroke_color + ';"></circle></g></svg>');
    }
    $(element).find('svg circle').css('fill', fill_color).css('stroke', stroke_color);

    // The following test should be done with
    // if(sibling_element.parent('.disabled').length > 0)
    // but for some reason this suddenly stopped working
    // (even when checking out a commit that was previously working, WTH)
    // so let's just use match since this is ok in this specific context.
    if(sibling_element.parent().attr('class').match(/disabled/)) {
        $(this).addClass('disabled');
    } else {
        $(this).removeClass('disabled');
    }
}

$('.keys dt').on('click', function() {
     // Do not trigger proxy-click actions if the current NVD3 chart has
     // been set up to not display a legend to start with, as there
     // would not be any legend to proxy to!
    var legendWrap = $(this).closest('.visualization .core').find('.nv-legendWrap');
    if(legendWrap.length > 0 && legendWrap.children().length > 0) {
        var key = $(this).data('key');
        var sibling_dot = $(this).closest('.visualization .core').find('.nv-legend .nv-series').find('text:contains("' + key + '")');
        sibling_dot.d3Click();
        $(this).closest('.keys').find('dt').each(update_legend_item);
        // reposition_scatterplot();
    }
});

$('.keys dt').on('dblclick', function() {
     // Do not trigger proxy-click actions if the current NVD3 chart has
     // been set up to not display a legend to start with, as there
     // would not be any legend to proxy to!
    var legendWrap = $(this).closest('.visualization .core').find('.nv-legendWrap');
    if(legendWrap.length > 0 && legendWrap.children().length > 0) {
        var key = $(this).data('key');
        var sibling_dot = $(this).closest('.visualization .core').find('.nv-legend .nv-series').find('text:contains("' + key + '")');
        sibling_dot.d3Dblclick();
        $(this).closest('.keys').find('dt').each(update_legend_item);
        // reposition_scatterplot();
    }
});
*/

/**
 * add d3Click method to jQuery (see http://stackoverflow.com/questions/9063383/how-to-invoke-click-event-programmaticaly-in-d3)
 */
jQuery.fn.d3Click = function () {
    this.each(function (i, e) {
        var evt = document.createEvent("MouseEvents");
        evt.initMouseEvent("click", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);

        e.dispatchEvent(evt);
    });
};

/**
 * add d3Dblclick method to jQuery (see http://stackoverflow.com/questions/9063383/how-to-invoke-click-event-programmaticaly-in-d3)
 */
jQuery.fn.d3Dblclick = function () {
    this.each(function (i, e) {
        var evt = document.createEvent("MouseEvents");
        evt.initMouseEvent("dblclick", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);

        e.dispatchEvent(evt);
    });
};

/**
 * Until we switch to Derby JS, let's use a simple in-page router
 * Basically when processing in-page target hash #example, we push it as
 * #p/example to window.location so that it is not interpreted as
 * a target on the page by the browser (it doesn't seem possible to
 * force Firefox to re-scroll the page to 0,0), so we trigger manually
 * the Bootstrap tab switch.
 */
$(document).ready(function() {
	// Bind a callback that executes when document.location.hash changes.
	$(window).bind( "hashchange", function(e) {
	  var route = location.hash.match(/^#p\/(.*)$/);
	  console.log(location.hash);
    if (route && route.length > 1 && route[1] !== '') {
      $('a[href="#' + route[1] + '"]').tab('show');
    }
	});
	$('.nav-tabs > li > a').on('click', function(e) {
	  var target_tab = e.delegateTarget.hash.substr(1);
	  location.hash = '#p/' + target_tab;
  	$(window).trigger( "hashchange" );
	  e.preventDefault();
	  return false;
	});
	$(window).trigger( "hashchange" );
});

/**
 * Get named parameter from HTTP GET query string
 * (see: https://stackoverflow.com/a/901144)
 */
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

(window.onpopstate = function () {
    var match,
        pl     = /\+/g,  // Regex for replacing addition symbol with a space
        search = /([^&=]+)=?([^&]*)/g,
        decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
        query  = window.location.search.substring(1);

    urlParams = {};
    while (match = search.exec(query))
       urlParams[decode(match[1])] = decode(match[2]);
})();
