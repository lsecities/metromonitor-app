// map configuration
var
    map_setup = {
        full: {
            translate: [310, 1000],
            scale: 800
        },
        city_focus: {
            translate: [150, 950],
            scale: 700
        }
    },
    margin = {top: 10, right: 20, bottom: 20, left: 20},
    w = 208 - margin.left - margin.right,
    h = 160 - margin.top - margin.bottom,
    data_years = _.range(1996, 2015), // data years
    // year slider
    slider_container_w = $('.timeseries-container').width() - margin.left - margin.right,
    year_slider_scale = d3.scale.linear().domain([_.first(data_years), _.last(data_years)]).range([0,slider_container_w]),
    // use range of years for slider ticks, adding a tick every three years
    year_slider_axis = d3.svg.axis().scale(year_slider_scale).tickValues(d3.range(_.first(data_years), _.last(data_years) + 1, 3)).tickFormat(d3.format("4d")),
    year_slider_container = d3.select('#slider-steps').append('svg').attr("width", "100%").attr('height', "50"),
    year_slider = year_slider_container.append("g").call(year_slider_axis).attr("transform", "translate(" + margin.left + ",0)"),
    slider_ticks_with_gaps = _.range(_.last(data_years) + 1, _.first(data_years) - 1, -3).reverse(),
    slider_ticks_tight = _.range(_.first(data_years), _.last(data_years)),
    slider_ticks = slider_ticks_with_gaps,
    indicators = { // indicators
        EMP: 'Employment',
        GVA: 'GVA'
    },
    grouped_sector_series = [ // sector series for grouped sectors
        { code: 'AGRIFF', label: 'Agriculture, Forestry & Fishing' },
        { code: 'PROCON', label: 'Production & Construction' },
        { code: 'DRHTC', label: 'Distribution, Retail, Hotels, Transport, Communications' },
        { code: 'FINBUS', label: 'Financial and Business Services' },
        { code: 'PAEDUHETC', label: 'Public Administration, Education, Health and Other Services' }
    ],
    all_sector_series = [ // sector series for the full range of sectors
        { code: 'Accommodation.FoodServices', label: 'Accommodation & food services' },
        { code: 'Administrative.SupportActivities', label: 'Administrative & support activities' },
        { code: 'Agriculture.Forestry.Fishing', label: 'Agriculture, forestry & fishing' },
        { code: 'Arts.Entertainment.Recreation', label: 'Arts, entertainment & recreation' },
        { code: 'Construction', label: 'Construction' },
        { code: 'Education', label: 'Education' },
        { code: 'Electricity.Gas.WaterSupply', label: 'Electricity, gas & water supply' },
        { code: 'Financial.InsuranceActivities', label: 'Financial & insurance activities' },
        { code: 'HumanHealth.SocialWork', label: 'Human health & social work' },
        { code: 'Information.Communication', label: 'Information & communication' },
        { code: 'Manufacturing', label: 'Manufacturing' },
        { code: 'Mining.Quarrying', label: 'Mining & quarrying' },
        { code: 'OtherServices', label: 'Other services' },
        { code: 'Professional.Scientific.TechnicalActivities', label: 'Professional, scientific & technical activities' },
        { code: 'PublicAdministration.Defence', label: 'Public administration & defence' },
        { code: 'RealEstateActivities', label: 'Real estate activities' },
        { code: 'Transportation.Storage', label: 'Transportation & storage' },
        { code: 'Wholesale.RetailTrade', label: 'Wholesale & retail trade' }
    ],
    all_sector_series_new = [
        { code: 'I', label: 'Accommodation & food services' },
        { code: 'N', label: 'Administrative & support activities' },
        { code: 'A', label: 'Agriculture, forestry & fishing' },
        { code: 'R', label: 'Arts, entertainment & recreation' },
        { code: 'F', label: 'Construction' },
        { code: 'P', label: 'Education' },
        { code: 'D_E', label: 'Electricity, gas & water supply' },
        { code: 'K', label: 'Financial & insurance activities' },
        { code: 'Q', label: 'Human health and social work' },
        { code: 'J', label: 'Information & communication' },
        { code: 'C', label: 'Manufacturing' },
        { code: 'B', label: 'Mining & Quarrying' },
        { code: 'S_U', label: 'Other services' },
        { code: 'M', label: 'Professional, scientific & technical activities' },
        { code: 'O', label: 'Public administration & defence' },
        { code: 'L', label: 'Real estate activities' },
        { code: 'H', label: 'Transportation & storage' },
        { code: 'G', label: 'Wholesale & retail trade' },
    ],
    sector_series = all_sector_series_new,
    city_types = [
        { code: 'A1', label: 'Leading European Capitals and Metropolises' },
        { code: 'A2', label: 'National Capitals and Metropolises' },
        { code: 'B1', label: 'Regional Service Centres' },
        { code: 'B2', label: 'Regional Innovation Centres' },
        { code: 'B3', label: 'Regional Centres with Growing Population' },
        { code: 'C1', label: 'Smaller Administrative Centres' },
        { code: 'C2', label: 'Smaller Centres with Growing Population' },
        { code: 'D1', label: 'Cities in the Process of Structural Adaption' },
        { code: 'D2', label: 'Less developed towns and cities' },
        { code: 'NN', label: 'Other cities' }
    ],
    all_countries = {
        AT: 'Austria',
        BE: 'Belgium',
        BG: 'Bulgaria',
        CH: 'Switzerland',
        CZ: 'Czech Republic',
        DE: 'Germany',
        DK: 'Denmark',
        EE: 'Estonia',
        ES: 'Spain',
        FI: 'Finland',
        FR: 'France',
        EL: 'Greece',
        HU: 'Hungary',
        IE: 'Ireland',
        IT: 'Italy',
        LT: 'Lithuania',
        LV: 'Latvia',
        NL: 'The Netherlands',
        NO: 'Norway',
        PL: 'Poland',
        PT: 'Portugal',
        RO: 'Romania',
        RU: 'Russia',
        SE: 'Sweden',
        SI: 'Slovenia',
        SK: 'Slovakia',
        TR: 'Turkey',
        UK: 'United Kingdom'
    },
    count_of_transborder_metro_regions, // computed from data, used in comment texts
    default_data_class = 'EMP', // default data class (Employment)
    active_data_class = default_data_class, // start with EMP as active indicator
    active_city = null, // no default city
    projection = d3.geo.miller(),
    path, // set up geo path with projection for map
    xy, // set up geo path with projection for cities
    svg, // add SVG element for main map
    g, // leaflet zoom-hide container
    countries, // container for countries (map)
    cities, // container for cities
    transform, // leaflet map overlay pane transform
    urlParams, // HTTP GET params
    showNUTSgrid = false, // whether to show the NUTS3 grid
    dataset = null, // global dataset variable
    dataset_typologies,
    timepoint = 2014,
    r = d3.scale.sqrt() // The radius scale for the centroids.
        .domain([0, 2e7])
        .range([0, 20]),
    fill = d3.scale.linear() // Fill color on a multilinear scale; using colorbrewer2.org for values
        .domain([-10, -5, 0, 5, 10])
        .range(["#2B83BA", "#ABDDA4", "#FFFFBF", "#FDAE61", "#D7191C"]),
    x = d3.scale.linear()
        .domain([_.min(data_years), _.max(data_years)])
        .range([0, w]),
    resilience_color = d3.scale.linear()
        .domain([ -10, 10])
        .range(['red', 'green']);

L.mapbox.accessToken = 'pk.eyJ1IjoibHNlY2l0aWVzIiwiYSI6IjJ6ZjA5UGMifQ.8y1yiWucAic55wg5YoiTGQ';
var map;

var citiesLayer, nutsLayer;

if($('#leaflet-map').length > 0) {
    map = L.mapbox.map('leaflet-map', 'lsecities.nmkn5fcj', {
    attributionControl: false,
    infoControl: true
    })
    .setView([51.505, 12], 4);

    var hash = new L.Hash(map);

    citiesLayer = L.geoJson(null, { pointToLayer: scaledPoint })
    .addTo(map);

    nutsLayer = new L.TopoJSON();

    map.on('zoomend', function (e) {
        zoomHandler();
    });

    citiesLayer.on('click', function(e) {
        active_city = e.layer.feature.properties.MetroRegion;

        update_city_visualization(e.layer.feature);
        draw_summary_growth_chart(active_city, active_data_class);
        draw_growth_chart(active_city, sector_series, active_data_class);
        draw_sector_percent_chart('.sector-percent.initial', active_city, 1998, active_data_class);
        draw_sector_percent_chart('.sector-percent.crisis', active_city, 2007, active_data_class);
        draw_sector_percent_chart('.sector-percent.final', active_city, 2014, active_data_class);
    });

}

do_visualizations();

function zoomHandler() {
    var current_zoom = map.getZoom();
    citiesLayer.setStyle(citiesSize);
    if(current_zoom > 8 && showNUTSgrid && !(map.hasLayer(nutsLayer))) {
        map.addLayer(nutsLayer);
    }

    if(current_zoom <= 8 && showNUTSgrid && map.hasLayer(nutsLayer)) {
        map.removeLayer(nutsLayer);
    }
}

function citiesSize(feature) {
    return { radius: pointRadius(feature), fillColor: pointColor(feature) };
}

function pointColor(feature) {
    return fill(feature.properties[active_data_class + '_' + timepoint + '_GR']);
}

function pointRadius(feature) {
    var radius = r(1000 * feature.properties['POPTOTT_' + timepoint]);
    if(map.getZoom() > 6) {
      return radius * 2;
    } else if(map.getZoom() > 8) {
      return radius * 3;
    } else {
      return radius;
    }
}

function scaledPoint(feature, latlng) {
    var pop = (feature.properties['POPTOTT_' + timepoint] / 1000).toFixed(2),
        gr = feature.properties[active_data_class + '_' + timepoint + '_GR'];
    return L.circleMarker(latlng, {
        radius: pointRadius(feature),
        fillColor: pointColor(feature),
        fillOpacity: 0.7,
        weight: 2,
        opacity: 1,
        color: '#fff'
    }); /*.bindPopup(
        '<h3>' + feature.properties.MetroRegion + '</h3>')*/;
}

function addTopoData(topoData){
  nutsLayer.addData(topoData);
  nutsLayer.setStyle({
    color: '#ccc',
    fill: false
  });
  // nutsLayer.addTo(map);
}

/*
citiesLayer.on('mouseover', function(e) {
    e.layer.openPopup();
});

citiesLayer.on('mouseout', function(e) {
    e.layer.closePopup();
});
*/


$(document).ready(function() {
  $('#date_display').html(timepoint);
});

function redraw() {
  timepoint = $('#slider').slider('value');
  performance(dataset, timepoint);
}

function do_visualizations() {
  var dataset_file = '/data/dataset.csv';

  d3.csv(dataset_file, function(flat_data){
    var data = _.map(flat_data, function(value, key, list) {
        return value;
    }),
    full_data = {
        type: "FeatureCollection",
        features: []
    };

    // TODO: grouped_sectors_data dataset was removed before first launch; we should
    // now calculate this data directly in R and add it to the main dataset

    dataset_typologies = data;

    full_data.features = _.map(_.reject(flat_data, function(city_row) { return parseInt(city_row.exclude) === 1; }), function(city_row, key, list) {

        return {
            id: city_row.MetroRegion,
            properties: city_row,
            type: "Feature",
            geometry: {
                type: "Point",
                coordinates: [
                    city_row.Longitude, city_row.Latitude
                ]
            }
        };
    });

    // add generated GeoJSON data as Leaflet layer
    if(citiesLayer) {
        citiesLayer.addData(full_data);
    }

    // add NUTS3 topojson data
    if(nutsLayer) {
        $.getJSON('/data/nuts3.topojson')
            .done(addTopoData);
    }

    /**
     * Count transborder MRs (used in comment texts)
     */
    count_of_transborder_metro_regions = _.filter(full_data.features, function(item) { return item.properties.Countries.match(/,/); }).length;

    /*
       Remove transborder MRs on MRvN charts, as this
       needs dedicated data and UX treatment
     */
    if($('.visualization#mrvnational20022007').length > 0 || $('.visualization#mrvnational20072012').length > 0) {
        full_data.features = _.reject(full_data.features, function(item) { return item.properties.Countries.match(/,/); });
    }

    console.log(full_data);

    dataset = full_data;

    // only compute and draw charts if on relevant page

    /**
     * MR vs National charts
     */
    if($(".visualization#mrvnational20022007 .chart svg").length > 0) {
        draw_typology_chart(
            dataset.features,
            {
                selector: '.visualization#mrvnational20022007 .chart svg',
                showLegend: true,
                x: {
                    variable: 'NATIONAL_AVG_GDP_GR_2002.2007',
                    domain_function: min_and_max_of_sample_for_variable,
                    label: 'Average annual growth rate of national GVA (2002-2007)'
                },
                y: {
                    variable: 'AVG_GDP_GR_2002.2007',
                    domain_function: min_and_max_of_sample_for_variable,
                    label: 'Average annual growth rate of metropolitan GVA (2002-2007)'
                },
                group: {
                    variable: 'Countries'
                },
                size: {
                    variable: 'POPTOTT_2014'
                }
            }
        );
    }

    if($(".visualization#mrvnational20082014 .chart svg").length > 0) {
        draw_typology_chart(
            dataset.features,
            {
                selector: '.visualization#mrvnational20082014 .chart svg',
                showLegend: true,
                x: {
                    variable: 'NATIONAL_AVG_GDP_GR_2008.2014',
                    domain_function: min_and_max_of_sample_for_variable,
                    label: 'Average annual growth rate of national GVA (2008-2014)'
                },
                y: {
                    variable: 'AVG_GDP_GR_2008.2014',
                    domain_function: min_and_max_of_sample_for_variable,
                    label: 'Average annual growth rate of metropolitan GVA (2008-2014)'
                },
                group: {
                    variable: 'Countries'
                },
                size: {
                    variable: 'POPTOTT_2014'
                }
            }
        );
    }

    /**
     * Resilience charts
     */
    if($(".visualization#resilience .chart svg").length > 0) {
        draw_typology_chart(
            dataset.features,
            {
                selector: '.visualization#resilience .chart svg',
                showLegend: false,
                x: {
                    variable: 'Resilience2',
                    domain_function: min_and_max_of_sample_for_variable,
                    label: 'R2 - Difference between post-recession and pre-recession growth'
                },
                y: {
                    variable: 'Resilience3',
                    domain_function: min_and_max_of_sample_for_variable,
                    label: 'R3 - Metropolitan vs national performance'
                },
                group: {
                    variable: 'Resilience1'
                },
                colorProperty: 'Resilience1',
                colorRange: [ 'red', 'green' ]
            }
        );
    }

    /**
     * City types charts
     */
    if($(".visualization#city-types .chart svg").length > 0) {
        draw_typology_chart(
            dataset.features,
            {
                selector: '.visualization#city-types .chart svg',
                showLegend: true,
                x: {
                    variable: 'AVG_EMP_GR_2008.2014',
                    domain_function: min_and_max_of_sample_for_variable,
                    label: 'Average employment growth rate (2008-2014)'
                },
                y: {
                    variable: 'AVG_GDP_GR_2008.2014',
                    domain_function: min_and_max_of_sample_for_variable,
                    label: 'Average GVA growth rate (2008-2014)'
                },
                group: {
                    variable: 'city_type'
                }
            }
        );
    }

    if($(".visualization#population .chart svg").length > 0) {
        draw_typology_chart(
            dataset.features,
            {
                selector: '.visualization#population .chart svg',
                showLegend: true,
                x: {
                    variable: 'AVG_EMP_GR_2008.2014',
                    domain_function: min_and_max_of_sample_for_variable,
                    label: 'Average employment growth rate (2008-2014)'
                },
                y: {
                    variable: 'AVG_GDP_GR_2008.2014',
                    domain_function: min_and_max_of_sample_for_variable,
                    label: 'Average GVA growth rate (2008-2014)'
                },
                group: {
                    variable: 'POP_DOMINANCE_2007'
                }
            }
        );
    }

    if($(".visualization#absolute-gva .chart svg").length > 0) {
        draw_typology_chart(
            dataset.features,
            {
                selector: '.visualization#absolute-gva .chart svg',
                showLegend: true,
                x: {
                    variable: 'AVG_EMP_GR_2008.2014',
                    domain_function: min_and_max_of_sample_for_variable,
                    label: 'Average employment growth rate (2008-2014)'
                },
                y: {
                    variable: 'AVG_GDP_GR_2008.2014',
                    domain_function: min_and_max_of_sample_for_variable,
                    label: 'Average GVA growth rate (2008-2014)'
                },
                group: {
                    variable: 'GDP_DOMINANCE_2007'
                }
            }
        );
    }

    if($(".visualization#gva-per-capita .chart svg").length > 0) {
        draw_typology_chart(
            dataset.features,
            {
                selector: '.visualization#gva-per-capita .chart svg',
                showLegend: true,
                x: {
                    variable: 'AVG_EMP_GR_2008.2014',
                    domain_function: min_and_max_of_sample_for_variable,
                    label: 'Average employment growth rate (2008-2014)'
                },
                y: {
                    variable: 'AVG_GDP_GR_2008.2014',
                    domain_function: min_and_max_of_sample_for_variable,
                    label: 'Average GVA growth rate (2008-2014)'
                },
                group: {
                    variable: 'GVA_PER_CAPITA_MR_VS_NATIONAL'
                }
            }
        );
    }

    if($(".visualization#capitals .chart svg").length > 0) {
        draw_typology_chart(
            dataset.features,
            {
                selector: '.visualization#capitals .chart svg',
                showLegend: true,
                x: {
                    variable: 'AVG_EMP_GR_2008.2014',
                    domain_function: min_and_max_of_sample_for_variable,
                    label: 'Average employment growth rate (2008-2014)'
                },
                y: {
                    variable: 'AVG_GDP_GR_2008.2014',
                    domain_function: min_and_max_of_sample_for_variable,
                    label: 'Average GVA growth rate (2008-2014)'
                },
                group: {
                    variable: 'is_capital_city'
                },
                colorProperty: 'is_capital_city',
                colorRange: [ '#2B83BA', 'orange' ]
            }
        );
    }

    /**
     * Broad sectors analysis page
     */

    if($(".visualization#sector-hvas .chart svg").length > 0) {
        draw_typology_chart(
            dataset.features,
            {
                selector: '.visualization#sector-hvas .chart svg',
                showLegend: false,
                x: {
                    variable: 'BROAD_SECTOR_HVAS_AVG_GDP_GR_1998.2007',
                    domain_function: range_function_for_sector_charts, // This function returns a constant, ad-hoc range specific to these Sector charts
                    label: 'Average annual metropolitan growth rate in the share of HVAS (1998-2007)'
                },
                y: {
                    variable: 'LOCATION_QUOTIENT_HVAS_1998',
                    domain_function: range_function_for_sector_charts, // This function returns a constant, ad-hoc range specific to these Sector charts
                    label: 'Location quotient of HVAS in 1998'
                },
                group: {
                    variable: 'BROAD_SECTOR_HVAS_AVG_GDP_GR_2008.2014'
                },
                colorProperty: 'BROAD_SECTOR_HVAS_AVG_GDP_GR_2008.2014',
                colorRange: [ 'red', 'green' ]
            }
        );
    }

    if($(".visualization#sector-manufacturing .chart svg").length > 0) {
        draw_typology_chart(
            dataset.features,
            {
                selector: '.visualization#sector-manufacturing .chart svg',
                showLegend: false,
                x: {
                    variable: 'BROAD_SECTOR_MANUFACTURING_AVG_GDP_GR_1998.2007',
                    domain_function: range_function_for_sector_charts, // This function returns a constant, ad-hoc range specific to these Sector charts
                    label: 'Average annual metropolitan growth rate in the share of Manufacturing (1998-2007)'
                },
                y: {
                    variable: 'LOCATION_QUOTIENT_MANUFACTURING_1998',
                    domain_function: range_function_for_sector_charts, // This function returns a constant, ad-hoc range specific to these Sector charts
                    label: 'Location quotient of Manufacturing in 1998'
                },
                group: {
                    variable: 'BROAD_SECTOR_MANUFACTURING_AVG_GDP_GR_2008.2014'
                },
                colorProperty: 'BROAD_SECTOR_MANUFACTURING_AVG_GDP_GR_2008.2014',
                colorRange: [ 'red', 'green' ]
            }
        );
    }

    if($(".visualization#sector-paeh .chart svg").length > 0) {
        draw_typology_chart(
            dataset.features,
            {
                selector: '.visualization#sector-paeh .chart svg',
                showLegend: false,
                x: {
                    variable: 'BROAD_SECTOR_PAEH_AVG_GDP_GR_1998.2007',
                    domain_function: range_function_for_sector_charts, // This function returns a constant, ad-hoc range specific to these Sector charts
                    label: 'Average annual metropolitan growth rate in the share of PAEH (1998-2007)'
                },
                y: {
                    variable: 'LOCATION_QUOTIENT_PAEH_1998',
                    domain_function: range_function_for_sector_charts, // This function returns a constant, ad-hoc range specific to these Sector charts
                    label: 'Location quotient of PAEH in 1998'
                },
                group: {
                    variable: 'BROAD_SECTOR_PAEH_AVG_GDP_GR_2008.2014'
                },
                colorProperty: 'BROAD_SECTOR_PAEH_AVG_GDP_GR_2008.2014',
                colorRange: [ 'red', 'green' ]
            }
        );
    }

    if($(".visualization#sector-rt .chart svg").length > 0) {
        draw_typology_chart(
            dataset.features,
            {
                selector: '.visualization#sector-rt .chart svg',
                showLegend: false,
                x: {
                    variable: 'BROAD_SECTOR_RT_AVG_GDP_GR_1998.2007',
                    domain_function: range_function_for_sector_charts, // This function returns a constant, ad-hoc range specific to these Sector charts
                    label: 'Average annual metropolitan growth rate in the share of Retail and Transport (1998-2007)'
                },
                y: {
                    variable: 'LOCATION_QUOTIENT_RT_1998',
                    domain_function: range_function_for_sector_charts, // This function returns a constant, ad-hoc range specific to these Sector charts
                    label: 'Location quotient Retail and Transport in 1998'
                },
                group: {
                    variable: 'BROAD_SECTOR_RT_AVG_GDP_GR_2008.2014'
                },
                colorProperty: 'BROAD_SECTOR_RT_AVG_GDP_GR_2008.2014',
                colorRange: [ 'red', 'green' ]
            }
        );
    }

    performance(dataset, null);

  });
}

function performance(json, timepoint) {

    if($("#leaflet-map").length === 0) {
        return;
    }

    citiesLayer.setStyle(citiesSize);

    /** re-enable after completing citiesLayer, attaching functions to city marker click
    viz.on("click", function(d, i) {
        update_city_visualization(d, i);
        draw_summary_growth_chart(active_city, active_data_class);
        draw_growth_chart(active_city, sector_series, active_data_class);
        // need to solve earliest data issue in R first
        draw_sector_percent_chart('.sector-percent.initial', active_city, 1998, active_data_class);
        draw_sector_percent_chart('.sector-percent.crisis', active_city, 2007, active_data_class);
        draw_sector_percent_chart('.sector-percent.final', active_city, 2014, active_data_class);

    });
    */

    if(active_city) {
        update_city_visualization(_.find(json.features, function(item) { return item.properties.MetroRegion === active_city; }));
    }
}

function update_city_visualization(d, i) {
        var indicator = active_data_class,
        city_field = d.properties.MetroRegion,
        city_label = '',
        metropolitan_region_cities_list = '',
        is_cross_border = null,
        growth_rate_this_year,
        growth_rate_text;

        $('.city-profile').removeClass('blank');

        // gently hide any intro text
        $('.disposable-text').remove();
        $('.hide-initially').removeClass('hide-initially');

        $('#city-id').html(city_field);

        active_city = city_field;

        // check whether this is a cross-border metro region
        if(d.properties.Countries.match(/,/)) {
            is_cross_border = true;
        }

        active_city = city_field;

        // if it is a Metro Region (/MR$/) then city name is Metro Region
        // and list of cities will be displayed below
        if(city_field.match(/MR$/)) {
            city_label = 'Metro region';
            metropolitan_region_cities_list = city_field.replace(/\s*MR$/, '');
        } else {
            city_label = city_field;
            metropolitan_region_cities_list = '&nbsp;';
        }

        // now update the info boxes
        $('.city-profile .city h1').html(city_label);
        $('.city-profile .cities').html(metropolitan_region_cities_list);

        // generated automatic comment text
        $('.city-profile .comment').html(generate_comment(timepoint, d, is_cross_border));

        // NEW DATASET
        // $('.city-profile .city .info').attr('style', 'background-color: ' + fill(d.properties[indicator + '_GR_' + (timepoint - 1) + '_' + timepoint]) + ';'); // DATASET: EMP|GDP_YYYY -- EMP|GVA_GR_YYYY_YYYY

        /**
         * If no data is available for this MR for this year, say so
         */
        growth_rate_this_year = d.properties[indicator + '_' + timepoint + '_GR'];
        if(isNaN(parseFloat(growth_rate_this_year))) {
          $('.city-profile .population').html('');
          $('.city-profile .performance').html('');
        } else {
          $('.city-profile .population').html("Population: <em>" + Math.round(parseFloat(d.properties['POPTOTT_' + timepoint]) * 1000) + '</em>');
          growth_rate_text = ' <em>' + d.properties[indicator + '_' + timepoint + '_GR'] + '% </em>';
          $('.city-profile .performance').html(indicators[indicator].ucFirst() + " growth rate:" + growth_rate_text);
        }

}

/**
 * generate comment text for current city
 */
function generate_comment(timepoint, d, is_cross_border) {
    var comment_text,
    requested_property = active_data_class + '_' + timepoint + '_GR',
    cities_in_country,
    nth_mr_in_country,
    nth_mr_in_sample,
    nth_mr_in_country_by_indicator,
    historic_data_for_indicator,
    years_of_negative_growth,
    is_growth_negative_this_year,
    last_index_of_NA_data,
    first_year_of_data,
    last_index_of_negative_growth_year,
    years_of_same_sign_growth,
    growth_or_recession,
    years_of_negative_growth_word,
    years_of_same_sign_growth_word;


    cities_in_country = _.map(_.sortBy(_.filter(dataset.features, function(item) { return item.properties.Countries == d.properties.Countries; }), function(item) { return parseFloat(item.properties.POPTOTT_2014); }), function(item) { return item.properties.POPTOTT_2014; }).reverse();

    if(!is_cross_border) nth_mr_in_country = cities_in_country.indexOf(d.properties.POPTOTT_2014) + 1;

    nth_mr_in_sample = _.map(_.sortBy(dataset.features, function(item) { return parseFloat(item.properties.POPTOTT_2014); }), function(item) { return item.properties.POPTOTT_2014; }).reverse().indexOf(d.properties.POPTOTT_2014) + 1;

    if(!is_cross_border) nth_mr_in_country_by_indicator = _.map(_.sortBy(_.filter(dataset.features, function(item) { return item.properties.Countries == d.properties.Countries; }), function(item) { return parseFloat(item.properties[requested_property]); }), function(item) { return item.properties[requested_property]; }).reverse().indexOf(d.properties[requested_property]) + 1;

    historic_data_for_indicator = _.map(_.range(data_years[0], timepoint + 1), function(year) { return d.properties[active_data_class + '_' + year + '_GR']; });

    last_index_of_NA_data = _.lastIndexOf(_.map(data_years, function(year) { return d.properties[active_data_class + '_' + year + '_GR']; }), '');

    first_year_of_data = last_index_of_NA_data < 0 ? data_years[0] : data_years[last_index_of_NA_data];

    /**
     * If historic_data_for_indicator (i.e. 1996 to current year) only contains emtpy values,
     * inform that we have no data.
     */
    if(isNaN(parseFloat(d.properties[active_data_class + '_' + timepoint + '_GR']))) {
      comment_text = 'No data available for ' + all_countries[d.properties.Countries] + ' until ' + (first_year_of_data + 1) + '.';
      return comment_text;
    }

    years_of_negative_growth = _.filter(historic_data_for_indicator, function(item) { return item < 0;}).length;

    is_growth_negative_this_year = d.properties[active_data_class + '_' + timepoint + '_GR'] > 0 ? false : true;

    last_index_of_growth_of_same_sign_as_this_year = _.lastIndexOf(_.map(_.range(first_year_of_data, timepoint + 1), function(year) { return d.properties[active_data_class + '_' + year + '_GR'] > 0; }), is_growth_negative_this_year);

    /**
     * If growth rate has never changed sign throughout the data years,
     * just say so
     */
    if(last_index_of_growth_of_same_sign_as_this_year < 0) {
      years_of_same_sign_growth = timepoint - first_year_of_data;
    }
    /**
     * Otherwise, calculate number of years of same-sign growth up to year
     * currently being displayed in visualisation.
     */
    else {
      years_of_same_sign_growth = timepoint - first_year_of_data - last_index_of_growth_of_same_sign_as_this_year;
    }

    growth_or_recession = is_growth_negative_this_year ? 'in recession' : 'growing without interruption';

    years_of_negative_growth_word = years_of_negative_growth !== 1 ? 'years' : 'year';

    years_of_same_sign_growth_word = years_of_same_sign_growth !== 1 ? 'years' : 'year';


    console.log(timepoint);

    if(!is_cross_border) {
      comment_text = '<p>' + d.properties.MetroRegion.ucFirst() + ' is the ' + getOrdinal(nth_mr_in_country) + ' metropolitan region in ' + all_countries[d.properties.Countries] + ' by population and the ' + getOrdinal(nth_mr_in_sample) + ' metropolitan region in the sample.</p>';
      comment_text += '<p>In ' + timepoint + ', it was the ' + getOrdinal(nth_mr_in_country_by_indicator) + ' metropolitan region in ' + all_countries[d.properties.Countries] + ' in terms of ' + indicators[active_data_class] + ' growth and has been ' + growth_or_recession + ' for ' + years_of_same_sign_growth + ' ' + years_of_same_sign_growth_word + '.';
    }
    /**
     * If cross border, just output
     * - number of transborder MRs in sample
     * - population ranking within sample
     * - years of same-sign growth
     * - years of negative growth since first year of data
     */
    else {
      comment_text = '<p>' + d.properties.MetroRegion.ucFirst() + ' is one of the ' + count_of_transborder_metro_regions + ' cross-border metropolitan regions in Europe and the ' + getOrdinal(nth_mr_in_sample) + ' metropolitan region in the sample by population.</p>';
      comment_text += '<p>In ' + timepoint + ', it had been ' + growth_or_recession + ' for ' + years_of_same_sign_growth + ' ' + years_of_same_sign_growth_word + ' in terms of ' + indicators[active_data_class] + ' growth.';
    }

    /**
     * Overall number of years with negative growth over the timespan of
     * first_year_of_data to year currently being displayed in visualisation.
     * If no recession has been experienced, show appropriate text.
     */
    if(years_of_negative_growth > 0) {
      comment_text += ' From ' + first_year_of_data + ' to ' + timepoint + ', it has known ' + years_of_negative_growth + ' ' + years_of_negative_growth_word + ' of negative growth rates.</p>';
    } else {
      comment_text += ' From ' + first_year_of_data + ' to ' + timepoint + ', it has known no negative growth.';
    }

    return comment_text;
}

function getOrdinal(n) {
   var s=["th","st","nd","rd"],
       v=n%100;
   return n+(s[(v-20)%10]||s[v]||s[0]);
}

function quantize(d) {
    return "q" + Math.min(8, ~~(data[d.id] * 9 / 12)) + "-9";
}

/**
 * Calculate lower and upper bounds for given variable of input dataset,
 * optionally adding an extra margin either way
 *
 * @param Object data The dataset
 * @param String variable Name of the variable to check
 * @param Float lower_margin (optional) lower margin
 * @param Float upper_margin (optional) upper margin
 * @return Array min and max, with margin if requested
 */
function min_and_max_of_sample_for_variable(data, variable, lower_margin, upper_margin) {
  lower_margin = typeof lower_margin !== 'undefined' ? lower_margin : 0;
  upper_margin = typeof upper_margin !== 'undefined' ? upper_margin : 0;

  var min = Math.floor(parseFloat(_.reduce(data, function(memo, item) {
    return parseFloat(item.properties[variable]) < parseFloat(memo.properties[variable]) ? item : memo ;
  }).properties[variable]) - lower_margin);

  var max = Math.ceil(parseFloat(_.reduce(data, function(memo, item) {
    return parseFloat(item.properties[variable]) > parseFloat(memo.properties[variable]) ? item : memo ;
  }).properties[variable]) + upper_margin);

  return [ min, max ];
}

/**
 * Return an ad-hoc min,max range for Sectors scatterplots
 *
 * @return Array min and max, constant and ad hoc
 */
function range_function_for_sector_charts() {
    return [ -3, 5 ];
}

$(function(){
    $('#slider').slider({
        value: timepoint,
        min: _.min(data_years),
        max: _.max(data_years),
        step: 1,
        slide: function(event, ui) {
            timepoint = ui.value;
            var current_city = $('#city-id').html();
            console.log(current_city);
            $('#date_display').html(timepoint);
            if(typeof dataset != 'undefined') {
                performance(dataset, timepoint);
            }
            if(current_city) {
                // $("[data-label='" + current_city + "']").d3Click();
                update_city_visualization(_.find(dataset.features, function(item) { return item.properties.MetroRegion === active_city; }));
            }
        }
    });
});

String.prototype.ucFirst = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

String.prototype.lcFirst = function() {
    return this.charAt(0).toLowerCase() + this.slice(1);
}

/**
 * Remove data rows that have NAs in columns used for X, Y or
 * group values
 *
 * @param Object data The source dataset
 * @param String x Name of the data column for the X variable
 * @param String y Name of the data column for the Y variable
 * @param String group Name of the data column for the group variable
 * @return Object The filtered dataset
 */
function reject_typology_data_rows_with_NAs(data, x, y, group) {
  return _.reject(data, function(item) {
    return item.properties[x] === 'NA' || item.properties[y] === 'NA' || item.properties[group] === 'NA';
  });
}
