function nvd3_legend_toggle_all(data, chart, recalculate_yDomain) {
    return function(e) {
        if (data.filter(function(d) { return !d.disabled }).length === 1)
            data = data.map(function(d) {
            d.disabled = false;
            return d;
        });
        else data = data.map(function(d,i) {
            //d.disabled = (i != e.seriesIndex);
            var allkeys = data.map(function(item) { return item.key; });
            d.disabled = (i != allkeys.indexOf(e.key));
            return d;
        });
    
        var state = {},
        values_domain = dataset_min_and_max(data),
        yDomain = [0, 250];
        
        /**
         * If asked to do so (recalculate_yDomain === true),
         * explicitly set a wider yDomain if values domain is wider
         * than default.
         */
        if(recalculate_yDomain && (values_domain[0] < yDomain[0] || values_domain[1] > yDomain[1])) {
            yDomain = [0, values_domain[1] - (values_domain[1] % 10) + 50 ];
            chart.yDomain(yDomain);
        }

        chart.state.disabled = data.map(function(d) { return !!d.disabled });
        chart.dispatch.stateChange(state);
    
        //selection.transition().call(chart);
        
        chart.update();
        // $('.keys dt').each(update_legend_item);
    };
}

/**
 * Return array of objects for NVD3's pie charts
 * 
 * @param string city The city for which to fetch data
 * @param integer year The year for which to fetch data
 * @param string indicator EMP or GVA
 * @return array A list of objects for NVD3's chart functions
 */
function pie_chart_data(city, year, indicator) {
  var city_data = _.find(dataset.features, function(item) { return item.properties.MetroRegion === active_city; });
  var data = _.map(
    all_sector_series_new,
    function(sector) {
      return { "label": sector.label, "value": city_data.properties['SECTOR_PERCENT_' + indicator.toUpperCase() + sector.code.toUpperCase() + '_' + year] };
    }
  );
  
  /**
   * If any of the values is not numeric (e.g. NA from R), just return
   * null and leave it to the caller to determine what to do.
   */
  var nans = data.filter(function(element, index, array) { 
      return parseFloat(element.value) != parseFloat(element.value);
    });
  if(nans.length > 0) {
      return null;
  } else {
    return data;
  }
}

function dataset_min_and_max(data) {
    var values_set = _.flatten(_.map(data, group_values));
    return [_.min(values_set), _.max(values_set)];
}

function group_values(group) {
    return _.map(group.values, function(value) {
        if(value.y) {
            return parseFloat(value.y);
        }
    });
}
