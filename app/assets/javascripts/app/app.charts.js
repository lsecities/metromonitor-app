// nvd3 graphs: employment and GVA
function draw_growth_chart(city, series_list, data_class){
    nv.addGraph(function() {  
        var
            chart = nv.models.lineChart(),
            data = line_chart_data(city, series_list, data_class, 2000),
            nvd3_legend_toggle_all_fn;

        chart.xAxis
            .axisLabel('Year')
            .tickFormat(d3.time.scale());
        chart.yAxis
            .axisLabel(line_chart_y_axis_label(data_class, data))
            .tickFormat(d3.format('f'));
        chart.tooltipContent(tooltip_content_value_in_year);
        
        chart.forceY(chart_ydomain(data, [0, 250]));
        
        nvd3_legend_toggle_all_fn = nvd3_legend_toggle_all(data, chart, false);
        
        chart.legend.dispatch.on('legendDblclick.toggle', nvd3_legend_toggle_all_fn);
        chart.legend.dispatch.on('legendClick.yaxis', reposition_line_chart_y_axis_label);
        chart.legend.dispatch.on('legendDblclick.yaxis', reposition_line_chart_y_axis_label);
          
        d3.select('#linechart').select('svg').remove();
        d3.select('#linechart').append('svg');

        d3.select('#linechart svg')
            .datum(data)
          .transition().duration(500)
            .call(chart);
            
        // make sure y axis label is visible
        reposition_line_chart_y_axis_label();
        
        nv.utils.windowResize(function() { d3.select('#linechart svg').call(chart); reposition_line_chart_y_axis_label() });
    
        return chart;
    });
};

/**
 * Compose label for y axis for line charts.
 * We assume that reading the x attribute of the first value of the
 * first series in the 'data' data structure will tell us the *second*
 * year of available data, from which we subtract 1 to display the first
 * year of available data.
 */
function line_chart_y_axis_label(data_class, data) {
    return indicators[data_class] + ' (' + (data[0].values[0].x - 1) + ' = 100)';
}

function reposition_line_chart_y_axis_label() {
    $('.nv-lineChart .nv-y .nv-axislabel').attr('y', '-50');
}

function tooltip_content_value_in_year(key, x, y, e, graph) {
    return '<h3>' + key + '</h3>' +
       '<p>' +  y + ' in ' + x + '</p>'
}

function chart_ydomain(data, base_domain) {
    var
        values_domain = dataset_min_and_max(data),
        yDomain = base_domain || [0, 250];

    // explicitly set a wider yDomain if values domain is wider than default
    if(values_domain[0] < yDomain[0] || values_domain[1] > yDomain[1]) {
        yDomain = [values_domain[0] - (values_domain[0] % 10 + 50), values_domain[1] - (values_domain[1] % 10) + 50 ]
    }
    // alternatively, use fixed classes of domains
    /*if(values_domain[1] >= 250 && values_domain[1] < 600) {
        yDomain = [0, 650];
    } else if (values_domain[1] >= 600 && values_domain[1] < 1000) {
        yDomain = [0, 1050];
    } */
        
    return yDomain;
}

var draw_summary_growth_chart = function(city, data_class){
    nv.addGraph(function() {
        var
            chart = nv.models.lineChart(),
            domelement = '.time-series svg.canvas',
            data = line_chart_data(city, [{ code: '', label: 'Total' }], data_class);
        
        chart.xAxis
            .axisLabel('Year')
            .tickFormat(d3.time.scale());
        chart.yAxis
            .axisLabel(line_chart_y_axis_label(data_class, data))
            .tickFormat(d3.format('.02f'));
        chart.showLegend(false);
        chart.tooltipContent(tooltip_content_value_in_year);
        
        chart.forceY(chart_ydomain(data, [50, 200]));
        
        d3.select(domelement)
            .datum(data)
          .transition().duration(500)
            .call(chart);
            
        // make sure y axis label is visible
        reposition_line_chart_y_axis_label();
        
        nv.utils.windowResize(function() { d3.select(domelement).call(chart); reposition_line_chart_y_axis_label(); });
        
        return chart;
    });
};

function line_chart_data(this_city, series_list, data_class, max_domain_range_threshold) {
    var data = _.map(series_list, function(series) {
        return {
            values: _.filter(_.map(data_years, function(year) {
                var city_data = _.find(dataset.features, function (city) { return city.id === this_city });
                /**
                 * Discard data points with missing data. Strictly speaking
                 * we should only discard data missing at the beginning of a data
                 * row, not holes inbetween valid data, but current dataset
                 * does not have such holes.
                 */
                if(city_data.properties[data_class + series.code + '_'  + year + '_B100']) {
                    return { x: year, y: parseFloat(city_data.properties[data_class + series.code + '_'  + year + '_B100']) };
                }
            }), function(d) { return typeof d !== 'undefined'; }),
            key: series.label// ,
            // yAxis: 1,
            //type: 'line'
        };
    });
    
    // If caller wants to filter out chart lines whose range is wider than
    // max_domain_range_threshold, apply filter
    if(typeof max_domain_range_threshold !== 'undefined') {
        data = _.filter(data, function(group) {
            var
                values = group_values(group)
                domain = Math.abs(_.max(values) - _.min(values));
            return domain < max_domain_range_threshold;
        });
    }
    
    // add base point if not present in data (not needed for older dataset of grouped sectors)
    // _.each(data, function(e) { e.values.unshift({x: e.values[0].x - 1, y: 100}); });
    
    return data;
}

function draw_sector_percent_chart(selector, city, year, indicator) {
    var chart_data = pie_chart_data(city, year, indicator);
    
    // if no valid data is available, just return
    if(null == chart_data) {
        return;
    }
    
  nv.addGraph(function() {
  var chart = nv.models.pieChart()
      .x(function(d) { return d.label })
      .y(function(d) { return d.value })
      .showLabels(false)     //Display pie labels
      .showLegend(false)
      .labelThreshold(.08)  //Configure the minimum slice size for labels to show up
      .labelType("key") //Configure what type of data to show in the label. Can be "key", "value" or "percent"
      .donut(true)          //Turn on Donut mode. Makes pie chart look tasty!
      .donutRatio(0.35)     //Configure how big you want the donut hole size to be.
      ;

    d3.select(selector + " svg")
        .datum(chart_data)
        .transition().duration(350)
        .call(chart);

  return chart;
});
  
}
