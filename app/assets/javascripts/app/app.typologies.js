function calculate_average(city, timespan, indicator) {
    var city_series = _.find(dataset, function(city) {
        return city.id === city;
    }),
    pattern = '^' + indicator + '_GR_',
    re = new RegExp(pattern);
     
    var indicator_series = _.filter(city_series.features, function(feature) {
        return feature.match(re);
    });
}

function data_typologies(data, properties) {
    var
    /**
     * the following map is an odd one - legacy of earlier times when
     * Excel-produced dataset cells were read as strings and contained
     * alien (=non-number) stuff; this should be unnecessary now,
     * but keeping for the moment - remove to shave off some time
     * when optimizing for performance
     */
    values_set = _.map(_.map(data, function(item) { return typeof properties.size != 'undefined' ? item.properties[properties.size.variable] : null; }), function(item) { if(item) { return parseInt(item.replace(/\D/g, '')); } }),
    /**
     * build the data structure needed for the chart functions
     */
    tmp = _.map(data, function(item) {
        return {
            label: item.properties['MetroRegion'],
            x: item.properties[properties.x.variable],
            y: item.properties[properties.y.variable],
            group: item.properties[properties.group.variable],
            color: properties.colorProperty ? compute_discrete_color_scale_from_set(data, item.properties[properties.colorProperty], properties.colorProperty, properties.colorRange) : null,
            //z: item.properties[properties.color],
            //size: (typeof properties.size != 'undefined' && typeof item.properties[properties.size.variable] != 'undefined') ? d3.scale.linear().range([1, 3]).domain([-10, 8])(item.properties[properties.size.variable]) : 1 
            size: (typeof properties.size != 'undefined' && typeof item.properties[properties.size.variable] != 'undefined') ? d3.scale.sqrt().range([1, 3]).domain([_.min(values_set), _.max(values_set)])(parseInt(item.properties[properties.size.variable].replace(/\D/g, ''))) : 1 
        };
    });
    
    /**
     * group by group property
     */
    tmp =  _.groupBy(tmp, 'group');
    
    tmp = _.map(tmp, function(value, key) {
        var datum = { key: key, color: value[0].color, values: _.map(value, function(value, key) { delete value.group; return value; } ) };
        if(datum.color === null) { delete datum.color; }
        return datum;
    });
    
    tmp = _.sortBy(tmp, function(item) { return item.key; });
    return tmp;
}

function compute_discrete_color_scale_from_set(data, i, colorProperty, colorRange) {
    categories = _.uniq(_.map(data, function(item) { return item.properties[colorProperty]; }).sort(function(a,b){return a - b}), true);
    if(typeof colorRange === 'undefined') { colorRange = ['red', 'green']; }
    
    var color = d3.scale.ordinal()
        .domain(categories)
        .range(d3.range(categories.length).map(d3.scale.linear()
        .domain([0, categories.length - 1])
        .range(colorRange)
        .interpolate(d3.interpolateLab)));
    
    return color(i);
}

/**
 * Generate and add to the NVD3 stack the function required to draw a
 * typology chart
 * 
 * @param Object data The dataset
 * @param Object metadata Metadata required to create axes, color scales, etc.
 */
function draw_typology_chart(source_data, metadata) {
    // first remove all rows that contain NAs in any of the applicable columns
    var
        valid_data = reject_typology_data_rows_with_NAs(source_data, metadata.x.variable, metadata.y.variable, metadata.group.variable),
        data = data_typologies(valid_data, metadata);
    
    nv.addGraph(function() {
        var
            xDomain,
            yDomain,
            showLegend = metadata.showLegend;
        
        if(typeof metadata.x.domain_function == 'undefined') {
            xDomain = [-4,5];
        } else {
            xDomain = metadata.x.domain_function(valid_data, metadata.x.variable);
        }
        
        if(typeof metadata.y.domain_function == 'undefined') {
            yDomain = [-4,6];
        } else {
            yDomain = metadata.y.domain_function(valid_data, metadata.y.variable);
        }
        
        if(typeof metadata.showLegend == 'undefined') { showLegend = true; metadata.showLegend = true; }
        
        var chart = nv.models.scatterChart()
            .showDistX(true)
            .showDistY(true)
            .fisheye(0)
            .xDomain(xDomain)
            .yDomain(yDomain)
            .showLegend(showLegend)
            .tooltipContent(function(key, x, y, obj) {  /*console.log(arguments);*/ return '<h3>' + obj.point.label + ' (' + key + ')</h3>' })
            //.color(nv.utils.defaultColor);
            //.color(function(key, x, y, z) {  console.log(arguments); });
            .color(d3.scale.category20().range()),
            stock_click_handler;
        
        var nvd3_legend_toggle_all_fn = nvd3_legend_toggle_all(data, chart);
        
        chart.xAxis
            .axisLabel(metadata.x.label)
            .tickFormat(d3.format('.02f%'));
        chart.yAxis
            .axisLabel(metadata.y.label)
            .tickFormat(d3.format('.02f%'));
        
/*
        chart.legend.dispatch.on('legendDblclick.toggle', nvd3_legend_toggle_all_fn);
        
        stock_click_handler = chart.legend.dispatch.legendClick;
        chart.legend.dispatch.on('legendClick.toggle', function(d,i, that) {
          d.disabled = !d.disabled;

          if (!data.filter(function(d) { return !d.disabled }).length) {
            data.map(function(d) {
              d.disabled = false;
              chart.wrap.selectAll('.nv-series').classed('disabled', false);
              return d;
            });
          }

          chart.state.disabled = data.map(function(d) { return !!d.disabled });
          chart.dispatch.stateChange(chart.state);

          chart.update();
          // $('.keys dt').each(update_legend_item);
        });
        
        chart.legend.dispatch.on('legendMouseover.toggle', function(series, index) {
            $(event.target).parents('.visualization').find('.keys dt[data-key="'+ series.key + '"]').addClass('focus');
        });
        chart.legend.dispatch.on('legendMouseout.toggle', function(series, index) {
            $(event.target).parents('.visualization').find('.keys dt[data-key="'+ series.key + '"]').removeClass('focus');
        });
        */
        d3.select(metadata.selector)
            .datum(data)
        .transition().duration(500)
            .call(chart);
    
        nv.utils.windowResize(chart.update);
    
        return chart;
    });
    
    nv.dispatch.on('render_end', function(e) {
        /**
         * Do not initialize proxy legend dots if this chart
         * has been configured not to display any legend as there would
         * be no legend to proxy to!
         */
        if(metadata.showLegend) {
            // $('.keys dt').each(update_legend_item);
        }
        // reposition_scatterplot();
    });
}
