// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require bootstrap
//= require bootstrap/tab
//= require vendor/jquery.cookie.js
//= require vendor/sly.js
//= require vendor/underscore.js
//= require vendor/d3.v3.js
//= require vendor/leaflet-hash.js
//= require vendor/d3.geo.projection.v0.min.js
//= require vendor/topojson.min.js
//= require vendor/nv.d3.js
//= require vendor/jquery.livequery.js
//= require vendor/jquery.color.js
//= require app/leaflet-topojson-layer.js
//= require app/app.nvd3.js
//= require app/app.charts.js
//= require app/app.typologies.js
//= require app/app.js
//= require app/urbanknowledge-ui.js
//= require_tree .
do_visualizations();
