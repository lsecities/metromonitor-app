European MetroMonitor
=====================

This is the Rails (backend) and JavaScript (frontend) app for the European
Metromonitor research project at LSE Cities.

The live version of the app is available at http://labs.lsecities.net/eumm/.

This was created as a Rails app as we were originally imagining that we would
need to run some sort of backend API, but as that didn't happen (all data
processing is done client-side) we basically use Rails only to serve assets and
compile HAML to HTML.

As of end of December 2014 the app will not receive further updates (except
bug fixes) and it would be advisable to quickly refactor the app into a static
site (e.g. to be compiled via gulp) to make hosting and management easier in
the future.

## Deploying to Heroku

We use Heroku to deploy this app.

To deploy, create a deploy branch off master (or whatever is the chosen deployable branch):

`git checkout master && git checkout -b deploy/live`

It may be necessary to update the Ruby version used (`.ruby-version` for rbenv)
when making any changes to the project after months or years since the previous
change, as the project is only receiving 'bug' fixes since 2015, and older Ruby
versions may not be usable anymore when setting up a dev environment to apply
the changes needed. When updating the rbenv environment, make sure that all
gems are up to date (or installed to start with, if using a clean environment):

`eval "$(rbenv init -)" && bundle update`

Clean & recompile static assets:

`export RAILS_ENV=production && rake assets:clean && rake assets:precompile`

Now add and commit precompiled assets:

`git add public/assets && git commit -m 'add precompiled assets'`

Push to Heroku's master branch (assuming the live app's repository on Heroku is
a remote called `live`):

`git push -f live deploy/live:master` (use -f if needed to force push - the
need for it will depend on previous commits of precompiled assets, etc.)

The deploy branch can now be discarded (we need to use -D as we don't want to
merge commits with precompiled assets back into master):

`git checkout master && git branch -D deploy/live`

Enjoy.

## License

CSV/JSON dataset (`/public/data` folder) are copyright respective rights holders with all rights reserved.

All other code: copyright (C) 2013-2015 LSE Cities <lse.cities@lse.ac.uk>

Author: Andrea Rota <a.rota@lse.ac.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

## App scaffolding

This application was generated with the [rails_apps_composer
gem](https://github.com/RailsApps/rails_apps_composer) provided by the
[RailsApps Project](http://railsapps.github.com/)

Recipes:

```
["controllers", "core", "email", "extras", "frontend", "gems", "git", "init", "models", "prelaunch", "railsapps", "readme", "routes", "saas", "setup", "testing", "views"]
```

Preferences:

```
{:git=>true, :railsapps=>"none", :dev_webserver=>"thin", :prod_webserver=>"thin", :database=>"sqlite", :templates=>"haml", :unit_test=>"test_unit", :integration=>"none", :fixtures=>"none", :frontend=>"none", :email=>"none", :authentication=>"none", :authorization=>"none", :form_builder=>"none", :starter_app=>"home_app", :ban_spiders=>true, :jsruntime=>false, :rvmrc=>true}
```
